from typing import List

from passthekeys.settings.common import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG: bool = True

ALLOWED_HOSTS: List[str] = ['*']

INTERNAL_IPS: List[str] = ['127.0.0.1']

app = INSTALLED_APPS.pop()
INSTALLED_APPS: List[str] = INSTALLED_APPS + [
    'django_extensions',
    'debug_toolbar',
    app,
]

MIDDLEWARE: List[str] = MIDDLEWARE + [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]

SHELL_PLUS = "ipython"
