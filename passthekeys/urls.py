from django.conf import settings
from postcode_districts.url import urlpatterns as postcodes_districts_urls

from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(postcodes_districts_urls)),
]

if settings.DEBUG:
    import debug_toolbar #noqa
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
