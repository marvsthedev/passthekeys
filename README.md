Install dependacies  

1: `pip install virtualenvwrapper` OR `sudo pip install virtualenvwrapper`  

2: `mkvirtualenv passthekeys` OR `mkvirtualenv -p /usr/bin/python3 passthekeys`  

3: `pip install -r dev-requirements.txt`  

4: `workon passthekeys`

Run Server  

1: `python manage.py runserver`  

From there you should be able to reach the urls describe in the test spec 

For front end test please look in `frontend_test/` 