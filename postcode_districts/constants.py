"""
Contains Postcode Districs constants
"""

LISTING_DATA_MAPPING = {
    'name': 4,
    'summary': 5,
    'description': 7,
    'picture_url': 17,
    'street': 37,
    'neighbourhood': 38,
    'neighbourhood_cleansed': 39,
    'city': 41,
    'state': 42,
    'zipcode': 43,
    'smart_location': 45,
    'country_code': 46,
    'country': 47,
    'latitude': 48,
    'longitude': 49,
    'is_location_exact': 50,
    'price': 60,
}
