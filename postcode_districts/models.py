"""
Django Models for passthekeys
"""
from typing import Union

from django.db import models


class Listing(models.Model):
    """
    Model for a listing
    """
    name = models.CharField(max_length=255)
    summary = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    picture_url = models.URLField(null=True, blank=True)
    street = models.CharField(max_length=255, null=True, blank=True)
    neighbourhood_raw = models.CharField(max_length=255, null=True, blank=True)
    neighbourhood_cleansed = models.CharField(max_length=255, null=True, blank=True)
    city = models.CharField(max_length=255, null=True, blank=True)
    state = models.CharField(max_length=255, null=True, blank=True)
    postcode = models.CharField(max_length=255, null=True, blank=True)
    smart_location = models.CharField(max_length=255, null=True, blank=True)
    country_code = models.CharField(max_length=255, null=True, blank=True)
    country = models.CharField(max_length=255, null=True, blank=True)
    latitude = models.CharField(max_length=255, null=True, blank=True)
    longitude = models.CharField(max_length=255, null=True, blank=True)
    is_location_exact = models.BooleanField(default=False, null=True, blank=True)

    # Only accepting two decimal points because thats all one gets when working with real money
    price = models.DecimalField(max_digits=19, decimal_places=2, null=True, blank=True)

    def __str__(self) -> str:
        """
        Returns string representation of object
        """
        return self.name

    @property
    def get_outcode(self) -> Union[str, None]:
        """
        Splits object postcode and returns the outcode from that
        """
        outcode = None
        if self.postcode:
            outcode = self.postcode.split()[0]

        return outcode
