"""
Custom Exception created to better handle errors and logging
"""


class IncompatibleValue(Exception):
    """
    Exception raised when an incompatible value is passed
    """
