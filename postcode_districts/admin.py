"""
Django Admin for passthekeys
"""
from django.contrib import admin

from postcode_districts.models import Listing

admin.site.register(Listing)
