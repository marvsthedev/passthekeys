"""
Postcode Districs utilities
"""
import csv
import os
from typing import Any, List, Union

from django.conf import settings
import requests

from postcode_districts.constants import LISTING_DATA_MAPPING
from postcode_districts.exceptions import IncompatibleValue
from postcode_districts.models import Listing


def convert_to_bool(value: str) -> Union[bool, Exception]:
    """
    Convert value from listing spreadsheet to bool
    """
    if value == 't':
        return True

    if value == 'f':
        return False

    raise IncompatibleValue('covert_to_bool() needs value to be either t or f')


def clean_number(value: str) -> float:
    """
    Clean number up from listing spreadsheet so that it can covert to a float
    and stored in the database
    """
    value = value.replace('$', '').replace(',', '')
    return round(float(value), ndigits=2)


def read_listing_data() -> None:
    """
    Reads listing.csv and returns the header along with the data
    """
    file_path = os.path.join(settings.PROJECT_DIR, 'listings.csv')
    with open(file_path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        data = []
        for row in csv_reader:
            data.append(row)

        return data


def commit_data_to_db(data: List[Listing]) -> None:
    """
    Will create a db model object for each listing
    """
    for row in data:
        # We need the name to create or get the object in the db
        name = row[LISTING_DATA_MAPPING['name']]
        listing, _ = Listing.objects.get_or_create(name=name)
        for key, _ in LISTING_DATA_MAPPING.items():
            # We have already proccessed the name so lets continue
            if key == 'name':
                continue

            value = row[LISTING_DATA_MAPPING[key]]
            if key == 'is_location_exact':
                value = convert_to_bool(value)

            if key == 'price':
                value = clean_number(value)

            # need to translate for postcodes as the model is uk but data is american
            if key == 'zipcode':
                key = 'postcode'

            setattr(listing, key, value)
            listing.save()


def get_listings_within_outcode(outcode: str) -> List[Listing]:
    """
    Will get listings within an outcode
    """
    listings = Listing.objects.all()
    listings_within_outcode = []
    for listing in listings:
        if listing.get_outcode:
            if outcode in listing.get_outcode:
                listings_within_outcode.append(listing)

    return listings_within_outcode


def get_average_daily_rate_of_listings(listings: List[Listing]) -> Any:
    """
    Will get average daily rate of a set of listings
    """
    avg_daily_rate = sum(listing.price for listing in listings) / len(listings)
    avg_daily_rate_rounded_to_two_decimal_points = round(avg_daily_rate, ndigits=2)
    return avg_daily_rate_rounded_to_two_decimal_points


def fetch_nearest_outcodes(outcode):
    """
    Go to postcodes.io and fetch the nearest outcodes to the outcode being passed
    """
    nearest_outcodes = requests.get(f'http://api.postcodes.io/outcodes/{outcode}/nearest')
    if nearest_outcodes:
        nearest_outcodes = nearest_outcodes.json()['result']

    return nearest_outcodes


def calculate_totals(outcodes):
    """
    Calculate total listings and total_avg_price. This is typically used in the nexus outcode view
    """
    total_listings = []
    for outcode in outcodes:
        listings = get_listings_within_outcode(outcode['outcode'])
        for listing in listings:
            total_listings.append(listing)

    total_avg_daily_rate = get_average_daily_rate_of_listings(total_listings)
    return total_listings, total_avg_daily_rate


def get_outcodes(nearest_outcodes):
    """
    Get data from all of the nearest outcodes
    """
    outcodes = []
    for outcode in nearest_outcodes:
        print(outcode)
        listings = get_listings_within_outcode(outcode['outcode'])
        if listings:
            avg_daily_rate_of_listings = get_average_daily_rate_of_listings(listings)
            outcodes.append({
                'outcode': outcode['outcode'],
                'distance': outcode['distance'],
                'listings_within_outcode': listings,
                'listings_count': len(listings),
                'avg_daily_rate_of_listings': avg_daily_rate_of_listings
            })
        else:
            outcodes.append({
                'outcode': outcode['outcode'],
                'distance': outcode['distance'],
                'listings_within_outcode': [],
                'listings_count': 0,
                'avg_daily_rate_of_listings': 0.00
            })

    return outcodes
