from django.test import TestCase

from postcode_districts import utils
from postcode_districts.exceptions import IncompatibleValue


class UtilsTestCase(TestCase):
    def setUp(self):
        pass

    def test_convert_to_bool_when_return_should_be_true(self):
        self.assertTrue(utils.convert_to_bool('t'))

    def test_convert_to_bool_when_return_should_be_false(self):
        self.assertFalse(utils.convert_to_bool('f'))

    def test_convert_to_bool_when_passing_an_incompatible_value(self):
        self.assertRaises(IncompatibleValue, lambda: utils.convert_to_bool('bad_value')[:1])

    def test_clean_number(self):
        value = utils.clean_number('$99.105')
        self.assertEqual(value, float('99.11'))
