"""
Postcode Districs views for passthekeys
"""
from django.http import HttpRequest, HttpResponse, HttpResponseNotFound
from django.shortcuts import render

from postcode_districts import utils


def get_outcode(request: HttpRequest, outcode: str) -> HttpResponse:
    """
    Return data on an outcode
    """
    listings = utils.get_listings_within_outcode(outcode)
    if not listings:
        return HttpResponseNotFound()

    avg_daily_rate = utils.get_average_daily_rate_of_listings(listings)
    context = {
        'listing_count': len(listings),
        'avg_daily_rate': avg_daily_rate,
        'outcode': outcode
    }
    return render(request, 'outcode.xml', context, content_type='application/xhtml+xml')


def get_nexus_outcode(request: HttpRequest, outcode: str) -> HttpResponse:
    """
    Return data on an outcode
    """
    nearest_outcodes = utils.fetch_nearest_outcodes(outcode)
    if not nearest_outcodes:
        return HttpResponseNotFound()

    total_listings, total_avg_daily_rate = utils.calculate_totals(nearest_outcodes)
    outcodes = utils.get_outcodes(nearest_outcodes)
    context = {
        'nexus_outcode': outcode,
        'total_listings_count': len(total_listings),
        'total_avg_daily_rate': total_avg_daily_rate,
        'outcodes': outcodes
    }
    return render(request, 'nexus_outcodes.xml', context, content_type='application/xhtml+xml')
