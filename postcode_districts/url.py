"""
Postcode Districs URLs for passthekeys
"""
from typing import Any, List

from django.urls import path

from postcode_districts.views import get_nexus_outcode, get_outcode

urlpatterns: List[Any] = [
    path('outcode/<outcode>/', get_outcode, name='get_outcode'),
    path('nexus/<outcode>/', get_nexus_outcode, name='get_nexus_outcode'),
]
