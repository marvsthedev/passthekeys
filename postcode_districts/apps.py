"""
Django App for passthekeys
"""
from django.apps import AppConfig


class PostcodeDistrictsConfig(AppConfig):
    name = 'postcode_districts'
